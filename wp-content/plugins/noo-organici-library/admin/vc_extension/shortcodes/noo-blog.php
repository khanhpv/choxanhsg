<?php

/**
 * Shortcode attributes
 * @var $style
 * @var $list_active
 * @var $description
 */

if( !function_exists('noo_shortcode_blog') ){

    function noo_shortcode_blog($atts){
        extract(shortcode_atts(array(
            'style_title'       =>  'one',
            'icon'              =>  '',
            'attach'            =>  '',
            'title'             =>  '',
            'description'       =>  '',
            'type_query'        =>  'cate',
            'categories'        =>  '',
            'tags'              =>  '',
            'include'           =>  '',
            'orderby'           =>  '',
            'posts_per_page'    =>  '',
            'columns'           =>  '2',
            'limit_excerpt'     =>   20,
            'show_loadmore'     =>  ''
        ),$atts));
        ob_start();
        wp_enqueue_script( 'vendor-carouFredSel' );
        wp_enqueue_script('vendor-isotope');
        ?>
        <?php if( $style_title == 'one' ): ?>
            <?php if( $title != '' || $description !='' ): ?>
                <div class="noo-sh-title noo-blog-title">
                    <?php if( isset($title) && !empty($title) ) ?><h2><?php echo esc_html($title);  ?></h2>
                    <?php if( isset($description) && !empty($description) ): ?><p><?php echo esc_html($description); ?></p><?php endif; ?>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="inheader-sh-title blog-sh-title2">
                <?php if(isset( $icon ) && !empty( $icon )): echo wp_get_attachment_image( esc_attr($icon),'',false,array('class'=>'icon') ); endif;?>
                <?php if( isset($title) && !empty($title) ): ?><h2><?php echo esc_html($title); ?></h2><?php endif; ?>
                <?php if( isset($attach) && !empty($attach) ): ?><h4><?php echo esc_html($attach); ?></h4><?php endif; ?>
                <?php if( isset($description) && !empty($description) ): ?><p class="ds"><?php echo esc_html($description); ?></p><?php endif; ?>
            </div>
        <?php endif; ?>
        <?php
        $class_columns = 'noo-md-4 noo-sm-6';
        if( $columns == 4 ){
            $class_columns = 'noo-md-3 noo-sm-6';
        }elseif( $columns == 2 ){
            $class_columns = 'noo-md-6 noo-sm-6';
        }
        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;

            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;

            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;

            case 'ralphabet':
                $orderby = 'title';
                break;

            default:
                $orderby = 'date';
                break;
        }

        if( is_front_page() || is_home()) {
         $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
        } else {
         $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        }

        $args = array(
            'post_type'         =>  'post',
            'orderby'           =>   $orderby,
            'order'             =>   $order,
            'posts_per_page'    =>   $posts_per_page,
            'paged'             =>   $paged,
        );

        if($type_query == 'cate'){
            $args['cat']   =  $categories ;
        }
        if($type_query == 'tag'){
            if($tags != 'all'):
                $tag_id = explode (',' , $tags);
                $args['tag__in'] = $tag_id;
            endif;
        }
        if($type_query == 'post_id'){
            $posts_var = '';
            if ( isset($include) && !empty($include) ){
                $posts_var = explode (',' , $include);
            }
            $args['post__in'] = $posts_var;
        }
        $query = new WP_Query($args);
        if( $query->have_posts() ):
            echo '<div class="masonry noo-blog">';
                echo '<div class="noo-row masonry-container">';
            while( $query->have_posts() ):
                $query->the_post();
                $format = get_post_format( get_the_ID() );
                $class_format = ' format-' . $format;
        ?>
                <div class="loadmore-item masonry-item <?php echo esc_attr($class_columns) . esc_attr($class_format); ?>">
                    <div class="blog-item">
                        <?php if ( $format == 'gallery' ) : ?>
                            <div class="content-featured">
                                <?php noo_organici_featured_gallery(); ?>
                            </div>
                        <?php elseif ( $format == 'audio' ) : ?>
                            <div class="content-featured">
                                <?php noo_organici_featured_audio(); ?>
                            </div>
                        <?php elseif ( $format == 'quote' ) : ?>
                            <?php
                                $quote = '';
                                $quote = noo_organici_get_post_meta(get_the_id() , '_noo_wp_post_quote', '');
                                if($quote == '') {
                                    $quote = get_the_title( get_the_id() );
                                }
                                $cite = noo_organici_get_post_meta(get_the_id() , '_noo_wp_post_quote_citation', '');
                                $alias = noo_organici_get_post_meta(get_the_id() , '_noo_wp_post_quote_alias', '');

                                $url_img = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ),'full');
                            ?>
                            <div class="content-featured">
                                <div class="blog-quote" style="background-image: url(<?php echo $url_img[0]; ?>);">
                                    <h3 class="content-title">
                                        <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(esc_html__('Permanent link to: "%s"', 'noo') , the_title_attribute('echo=0'))); ?>">
                                            <?php echo esc_html($quote); ?>
                                        </a>
                                    </h3>
                                    <cite>- <?php echo esc_html($cite);?> -</cite>
                                    <span class="alias">(<?php echo esc_html($alias);?>)</span>
                                </div>
                            </div>
                        <?php else: ?>
                            <a class="blog-thumbnail" href="<?php the_permalink() ?>">
                                <?php
                                    if ( $format == 'video' ) {
                                        noo_organici_featured_video();
                                    } elseif ( $format == 'image' ) {
                                        noo_organici_featured_default();
                                    } else {
                                        the_post_thumbnail('noo-thumbnail-square');
                                    }
                                ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( $format != 'quote' ) : ?>
                        <div class="blog-description">
                            <span class="cat">
                                <?php the_category('/'); ?>
                            </span>
                            <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                            <?php if ( $format != 'video' && $format != 'audio' ) : ?>
                            <p class="blog_excerpt">
                                <?php
                                    $excerpt = get_the_excerpt();
                                    $trim_ex = wp_trim_words($excerpt,esc_attr($limit_excerpt),'...');
                                    echo esc_html($trim_ex);
                                ?>
                            </p>
                            <?php endif; ?>
                            <a class="view-more" href="<?php the_permalink() ?>"><?php echo esc_html__('View more','noo'); ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
        <?php
            endwhile;
            echo '</div>';
            ?>

            <?php if( 1 < $query->max_num_pages && 'yes' == $show_loadmore ):?>
                <div class="loadmore-action">
                    <a href="#" class="btn-loadmore btn-primary" title="<?php _e('Load More','noo')?>"><?php _e('Load More','noo')?></a>
                    <div class="noo-loader loadmore-loading"><span></span><span></span><span></span><span></span><span></span></div>
                </div>
                <?php  noo_organici_pagination_normal(array(),$query)?>
            <?php endif;?>
            <?php
        echo '</div> <!-- masonry -->';
        endif; wp_reset_postdata();
        ?>
        
        <?php
        $blog = ob_get_contents();
        ob_end_clean();
        return $blog;
    }
    add_shortcode('noo_blog','noo_shortcode_blog');

}