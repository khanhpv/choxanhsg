<?php
/* *
 * This shortcode display product by style grid
 */

if( !function_exists('noo_shortcode_product_grid') ){
    function noo_shortcode_product_grid($atts){
        extract(shortcode_atts(array(
            'icon'           =>  '',
            'title'          =>  '',
            'description'    =>  '',
            'cat'            =>  '',
            'style'          =>  'one',
            'columns'        =>  '4',
            'orderby'        =>  'latest',
            'limit_excerpt'  => '',
            'posts_per_page' =>  8
        ),$atts));
        ob_start();
        wp_enqueue_script('vendor-imagesloaded');
        wp_enqueue_script('isotope');
        wp_enqueue_script('portfolio');
        $class= 'product-style';
        if( $style == 'two' ){
            $class= 'product-style2';
        }
        ?>
        <div class="noo-product-grid-wrap woocommerce <?php echo esc_attr($class) ?>">
            <?php if( $title != '' || $description !='' ): ?>
            <div class="noo-sh-title">
                <?php if( isset($icon) && !empty($icon) ): echo wp_get_attachment_image($icon,'full'); endif; ?>
                <?php if( isset($title) && !empty($title) ) ?><h2><?php echo esc_html($title);  ?></h2>
                <?php if( isset($description) && !empty($description) ): ?><p><?php echo esc_html($description); ?></p><?php endif; ?>
            </div>
            <?php endif; ?>
            <div class="noo-product-filter masonry-filters">
                <ul class="noo-header-filter" data-option-key="filter">
                    <li>
                        <a data-option-value="*" href="#all" class="selected">
                            <img width="30" height="26" src="<?php echo esc_url(NOO_PLUGIN_ASSETS_URI.'/images/organicfood.png'); ?>" alt="">
                            <span><?php echo esc_html__('All products','noo') ?></span>
                        </a>
                    </li>
                    <?php
                        $args_cat = array(
                            'type'      =>  'product',
                            'taxonomy'  =>  'product_cat'
                        );
                        if( isset($cat) && $cat != 'all' && !empty($cat) ){
                            $args_cat['include'] = $cat;
                        }
                        $categories = get_categories( $args_cat );
                        if( isset($categories) && !empty( $categories ) ):
                            foreach( $categories as $cats ): ?>
                                <li>
                                    <a data-option-value=".<?php echo esc_attr($cats->slug); ?>" href="#<?php echo esc_attr($cats->slug); ?>">
                                      <?php
                                      $thumbnail_id = get_woocommerce_term_meta( $cats->term_id, 'thumbnail_id', true );
                                      $image = wp_get_attachment_url( $thumbnail_id );
                                      if ( $image ) {
                                          echo '<img width="30" height="26" src="' . esc_url($image) . '" alt="'.esc_attr($cats->name).'" />';
                                      }
                                      ?>
                                        <span><?php echo esc_html($cats->name); ?></span>
                                    </a>
                                </li>
                            <?php endforeach;
                        endif;
                    ?>

                </ul>
            </div>
            <div class="noo-product-grid products noo-row product-grid noo-grid-<?php echo esc_attr($columns); ?>">
                <?php
                $order = 'DESC';
                switch ($orderby) {
                    case 'latest':
                        $orderby = 'date';
                        break;

                    case 'oldest':
                        $orderby = 'date';
                        $order = 'ASC';
                        break;

                    case 'alphabet':
                        $orderby = 'title';
                        $order = 'ASC';
                        break;

                    case 'ralphabet':
                        $orderby = 'title';
                        break;

                    default:
                        $orderby = 'date';
                        break;
                }
                $args = array(
                    'post_type'         =>  'product',
                    'orderby'           =>   $orderby,
                    'order'             =>   $order,
                    'posts_per_page'    =>   $posts_per_page
                );


                if( isset($cat) && $cat != 'all' && !empty($cat) ){
                   $new_id = explode(',',$cat);
                    $new_cat = array();
                    foreach($new_id as $id){
                        $new_cat[] = intval($id);
                    }
                    $args['tax_query'][] = array(
                        'taxonomy'  =>  'product_cat',
                        'field'     =>  'term_id',
                        'terms'     =>   $new_cat
                    );
                }

                $query = new WP_Query($args) ;
                if( $query->have_posts() ):
                    while( $query->have_posts() ): $query->the_post();

                        ?>
                        <?php wc_get_template_part( 'content', 'product' ); ?>
                    <?php   endwhile;
                endif; wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php
        $grid = ob_get_contents();
        ob_end_clean();
        return $grid;
    }
    add_shortcode('noo_product_grid','noo_shortcode_product_grid');
}