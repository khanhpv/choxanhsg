<?php
// [Shortcode] Noo Map
// ============================
if( !function_exists( 'noo_shortcode_map' ) ) {
    function noo_shortcode_map ( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'latitude'       => '',
            'longitude'      => '',
            'icon'           => '',
            'height'         => '500',
            'title'          => '',
            'description'    => '',
            'location'       => '',
            'email'          => '',
            'phone'          => '',
            'website'        => ''
        ), $atts ) );

        if ( isset($latitude) && !empty($latitude) ) :
            wp_enqueue_script( 'google-map' );
            wp_enqueue_script( 'google-map-custom' );
        endif;

        if( $icon =='' ){
            $image = NOO_ASSETS_URI.'/images/marker-icon.png';
        }else{
            $image = wp_get_attachment_thumb_url($icon);
        }

        ob_start();
        
        ?>
        <div class="google-map">
            <div id="googleMap" data-icon="<?php echo esc_url($image); ?>" data-lat="<?php echo esc_attr($latitude); ?>" data-lon="<?php echo esc_attr($longitude); ?>" <?php if( isset($height) && !empty($height) ): ?> style="height: <?php echo esc_attr($height).'px'; ?>" <?php endif; ?>></div>
            <div class="noo-address-info-wrap">
                <div class="noo-container">
                    <div class="address-info">
                        <h3><?php echo esc_attr($title); ?></h3>
                        <p><?php echo esc_attr($description); ?></p>
                        <ul>
                            <li><i class="fa fa-map-marker"></i><span><?php echo esc_attr($location); ?></span></li>
                            <li><i class="fa fa-envelope"> </i><span><?php echo esc_attr($email); ?></span></li>
                            <li><i class="fa fa-phone"> </i><span><?php echo esc_attr($phone); ?></span></li>
                            <li><i class="fa fa-globe"> </i><span><a href="<?php echo esc_url($website); ?>"><?php echo esc_url($website); ?></a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> <!-- /.google-map -->
        <?php
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}
add_shortcode( 'noo_map', 'noo_shortcode_map' );